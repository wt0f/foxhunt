' HF CW Fox Hunt Beacon
'
' ko7m - Jeff Whitlatch
'

con
  _CLKMODE = XTAL1 + PLL16X
  _XINFREQ = 5_000_000

'' The following constants are used in generating random numbers and to
'' fine tune the timing accuracy of a delay function.

  Seed = 31414
  WMin = 381

'' Each transmitter gets a 6 second slot for transmitting, chosen at random
'' and the transmitters re-sync random number generation every two minutes.

  '' Sync Each Ten Minutes So that Others can be be started Later, by watching the wall clock
  TimeSync  = 18            '' Seconds between time syncs (2 min)
  SlotTime  = 6                 '' Seconds
  TimeSlots = TimeSync / SlotTime '' Number of time slots (in this case 20)

'' We transmit at 15 words per minute on a frequency of 10.1395 MHz with a
'' tone offset of 600 hz.  Here we also define which pin is used to generate
'' the RF at this frequency.  Any available pin may be chosen for this task.

  wpmInit      =         15      '' Initial code speed in words per minute
  defaultFreq  = 10_139_500      '' Default transmit frequency
  defaultTone  =        600      '' Offset from transmit frequency
  defaultRFPin =         16      '' RF output default pin

'' This data section defines an array of bytes used to identify each transmitter.
'' These are the letters sent (repeated 5 times) during each time slot.
'' For example the transmitter with the serial number 2 will send VVVVV in morse
'' code in randomly chosen time slots.

'' Here we also declare an object instance of the frequency synthesizer object
'' used to generate RF energy.  This module is provided as part of the standard
'' modules available with the development tools for the Propeller processor.
'' The source code is provided and nothing is done here that you cannot do
'' yourself in your own code.

dat
  Ids      BYTE "BFGL"           '' Equal Length Single Character IDs, one for each SN

obj
  Freq  : "Synth"
  Term  : "Parallax Serial Terminal"
  
'' Local variables are now defined.

var
  long TimeSlot
  long Rand
  long WPM
  long Frequency                '' Current frequency
  long toneFreq                 '' Offset from frequency in Hz
  
  byte RFPin
  byte ditTime                  '' Time in milliseconds for a dit (dot)
  byte SN                       '' Serial number (set by switches)
  long Stack[128]
  BYTE Cog

'' Public functions now follow.  Main performs initialization and
'' then runs the CW beacon routines.

pub Main
  Init
  doHFFox

'' The Start and Stop functions allow starting this code up on a different
'' core and stopping it.  This allows me to implement both the HF and VHF
'' transmitters in a way that they can function simultaneously.

PUB Start : fSuccess 
  Term.Start(115_200)
  Term.Str(string("Starting KO7M HF code", 13))
  fSuccess := (Cog := cognew(Main, @Stack) + 1) > 0
  if fSuccess 
    Term.Str(string("Started on Cog ", 13))
  else 
    Term.Str(string("Failed to start!", 13))
    
    
PUB Stop
  if Cog
    cogstop(Cog~ - 1)
 
'' This is the main function that does all the magic.  It schedules the random
'' slot selections and at the appropriate time sends the identifier character
'' in morse code based on the serial number.

'' During slots 1 and 5, the fox with serial number 0 transmits.  Slots 2 and 6,
'' serial number 1 transmits and so forth.  During slot 9 all foxes transmit and
'' during slot 0, nobody transmits.  The effect is that you may hear zero to four
'' foxes all talking at once during each time slot.

pri doHFFox | Task, C
  C := cnt

  Term.Str(string("doHFFox()", 13))
  repeat
    waitcnt(C += (clkfreq * SlotTime) #> WMin)          '' Wait for our slot time
    C := cnt                                            '' Get the current clock value

    SN := 2                                             '' Serial number is hard coded - should be on switch input

    if TimeSlot == 0                                    '' Reset Sequence
      Rand := Seed

    Task := ||?Rand // 10                               '' Random task number (only use last digit) '

    case (Task)
      1,5:
        if SN == 0
          sendId(SN)                    '' Send my ID at different times based on serial number and task
      2,6:
        if SN == 1
          sendId(SN)
      3,7:
        if SN == 2
          sendId(SN)
      4,8:
        if SN == 3
          sendId(SN)
      9:
        sendId(SN)                                      '' Everybody sends
      0:
        sendNothing                      '' Noboday sends (silence is heard for time required to identify) 

    TimeSlot := (TimeSlot + 1) // TimeSlots

'' Now, we have the private functions.  Init handles initialization setting
'' the frequency, morse code speed, RF pin to use, etc.

pri Init
  TimeSlot  := 0
  Rand      := Seed
  WPM       := wpmInit
  ditTime   := 1200 / WPM                        '' Calculate dit time based on 50 dit duration standard
  Frequency := defaultFreq
  toneFreq  := defaultTone
  RFPin     := defaultRFPin

'' Now we have the various "send" functions that handle the actual morse code
'' transmissions.  sendId send the single identifier character based on serial
'' number 5 times.  sendNothing does exactly that.  sendCode works with
'' sendSymbol to allow morse code transmission of strings of characters.

pri sendId(_SN)
  Term.Str(string("sending fox id", 13))
  repeat 3
    sendSymbol(Ids[_SN])                                '' Send the associated ID for the serial number

pri sendNothing

pri sendCode(stringptr)
  repeat strsize(stringptr)
    sendSymbol(byte[stringptr++])                       '' Send each symbol of the stream

'' sendSymbol is the main function that provides the conversion of an ascii
'' character into a series of morse code symbols that are used to key the RF
'' energy on and off forming morse code characters.  The character itself is
'' used as the index into a lookup table of morse code symbols for that character.
'' The function then forms properly constructed morse code and keys the transmitter.

pri sendSymbol(char) | cwBits, dotsToWait, iCodes
  if char == " "                                        '' Handle space character as 7 dot times
    dotsToWait := 5
    delay(dotsToWait * ditTime)
  else
    if char => "a" and char =< "z"                      '' Convert lower case to upper case
      char := char - "a" + "A"
    iCodes := lookdown(char: "!", $22, "$&'()+,-./0123456789:;=?@ABCDEFGHIJKLMNOPQRSTUVWXYZ_")
    '' If unsupported character, ignore it
    if iCodes == 0
      return
    cwBits := cwCodes[iCodes - 1]                       '' Grab the CW bit codes for this symbol 

    repeat while cwBits > 1
      if cwBits & 1
        dotsToWait := 3         '' dah
      else
        dotsToWait := 1         '' dit

      keyDown
      delay(dotsToWait * ditTime)
      keyUp                       '' stop sending
      delay(ditTime)              '' one dot time between symbols
      cwBits >>= 1                '' get the next symbol (dit or dah)

  delay(ditTime * 2)            '' two more dot times to give 3 dot character spacing

'' The following functions are use to control the frequency synthesizer
'' to actually generate RF energy.  sendTone turns on an RF carrier at the
'' requested frequency plus a tone offset value.  noTone turns off that RF
'' carrier.  These functions are used by keyDown and keyUp to generate morse
'' code keying transitions.  delay is a general purpose function to delay
'' for the specified number of milliseconds.

pri sendTone(tone)
  Freq.Synth("A", RFPin, Frequency + tone)

pri noTone
  Freq.Synth("A", RFPin, 0)

pri keyDown
  sendTone(toneFreq)            '' Sidetone or transmit

pri keyUp
  noTone                        '' Stop sidetone or transmit

pri delay(Duration)
  waitcnt(((clkfreq / 1_000 * Duration - 3932) #> WMin) + cnt)

'' This data block is used  to define the morse code characters as described
'' in the comments below.  Each character is stored in a byte where each one
'' bit represents a dash (or dah) and a zero bit indicates a dot (or dit) in
'' morse code.

dat
  '' Morse code tables below are encoded as "1" being a dah and "0" being a dit.  The end of the character
  '' is signified by the most significant "1" bit.  Thus, while shifting the byte value right, we can easily
  '' test for the end of character condition by checking to see if the current value is > 1.

  ''            !         "         $         &         \'         (         )         +         ,         -         .         /
  cwCodes byte %1110101, %1010010, %11001000,%100010,  %1011110, %101101,  %1101101, %101010,  %1110011, %1100001, %1101010, %101001
  ''            0         1         2         3         4         5         6         7         8         9
          byte %111111,  %111110,  %111100,  %111000,  %110000,  %100000,  %100001,  %100011,  %100111,  %101111
  ''            :         ;         =         ?         @
          byte %1000111, %1010101, %110001,  %1001100, %1010110
  ''            A         B         C         D         E         F         G         H
          byte %110,     %10001,   %10101,   %1001,    %10,      %10100,   %1011,    %10000
  ''            I         J         K         L         M         N         O         P        Q        R
          byte %100,     %11110,   %1101,    %10010,   %111,     %101,     %1111,    %10110,  %11011,  %1010
  ''            S         T         U         V         W         X         Y         Z        _
          byte %1000,    %11,      %1100,    %11000,   %1110,    %11001,   %11101,   %10011,  %1101100

